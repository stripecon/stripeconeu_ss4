/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const { DefinePlugin } = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const resolve = require('./webpack.resolve').forWebpack;

const pkg = require('./package.json');

module.exports = env => ({

  entry: {
    home: ['@babel/polyfill', 'pages/home.js'],
    page: ['@babel/polyfill', 'pages/page.js']
  },

  output: {
    path: path.resolve(__dirname, './build'),
    filename: `[name].${pkg.version}.js`,
    publicPath: '/resources/themes/virtual-stripecon/build/'
  },

  mode: 'production',

  resolve,

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: file => (
          /node_modules/.test(file)
          && !/\.vue\.js/.test(file)
        ),
        use: {
          loader: 'babel-loader',
          options: {
            plugins: [
              '@babel/plugin-proposal-object-rest-spread',
              '@babel/plugin-syntax-dynamic-import'
            ],
            presets: [['@babel/preset-env', { modules: false }, '@babel/preset-stage-3']]
          }
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'vue-style-loader'
          },
          {
            loader: MiniCSSExtractPlugin.loader
          },
          {
            loader: 'css-loader'
          }
        ]
      },
      {
        test: /\.less$/,
        use: [
          {
            loader: 'vue-style-loader'
          },
          {
            loader: MiniCSSExtractPlugin.loader
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'less-loader'
          }
        ]
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      }
    ]
  },

  plugins: [
    new DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(env.NODE_ENV)
      }
    }),

    new VueLoaderPlugin(),

    // Optimize and extract styles
    new OptimizeCssAssetsPlugin(),
    new MiniCSSExtractPlugin({
      filename: `[name].${pkg.version}.css`
    }),

    // Compression, create gzip and .br files
    new CompressionPlugin({
      algorithm: 'gzip'
    }),

    new BrotliPlugin()
  ]
});
