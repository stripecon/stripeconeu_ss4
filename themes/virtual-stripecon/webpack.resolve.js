const path = require('path');

const alias = {
  vue$: 'vue/dist/vue.common.js', // Use the standalone version of vue, see https://vuejs.org/v2/guide/installation.html#NPM
  styles: path.resolve(__dirname, './styles'),
  utils: path.resolve(__dirname, './js/utils'),
  components: path.resolve(__dirname, './js/components'),
  services: path.resolve(__dirname, './js/services'),
  mixins: path.resolve(__dirname, './js/mixins'),
  pages: path.resolve(__dirname, './js/pages'),
  src: path.resolve(__dirname, './js'),
  node_modules: path.resolve(__dirname, './node_modules')
};

const extensions = ['.js', '.vue', '.json', '.less'];

module.exports = {
  forWebpack: { alias, extensions },
  forEsLint: {
    'import/resolver': {
      alias: {
        map: Object.keys(alias).map(key => [key, alias[key]]),
        extensions
      }
    }
  }
};
