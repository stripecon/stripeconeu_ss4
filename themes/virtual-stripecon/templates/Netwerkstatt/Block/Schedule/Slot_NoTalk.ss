<%-- one talk: normal talk --%>
<div class="slot">
    <div class="slot__desc">
      <div class="slot-headline">
        <div class="slot-time">
            $Time.Format('HH:mm')
        </div>
        <div class="slot-title">
            $Title
        </div>
      </div>

      $Description
    </div>
</div>

