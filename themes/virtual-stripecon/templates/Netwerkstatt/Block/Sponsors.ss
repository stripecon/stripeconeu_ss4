<div class="content-element__sponsors<% if $Style %> $StyleVariant<% end_if %> center">
    <% if $ShowTitle %>
      <h2 class="section-headline">
          $Title
      </h2>
    <% end_if %>
    $HTML

    <% loop $AllActiveSponsors.GroupedBy('LevelID') %>
      <section>
        <h3 class="center">$Children.First.Level.Title</h3>

        <div class="sponsoritems-wrapper">
            <% loop $Children.Sort('TitleUppercase') %>
                <div class="sponsoritem sponsoritem--$SponsorLevelSuffix">
                  <div class="sponsoritem-inner">
                    <h4 class="sponsoritem__title">
                        <% if $Link %><a href="$Link.URL" target="_blank"><% end_if %>
                        $Title
                        <% if $Link %></a><% end_if %>
                    </h4>
                    <div class="sponsoritem__logo">
                        <% if $Link %><a class="clear" href="$Link.URL" target="_blank"><% end_if %>
                        $Logo.FitMax(520,340)
                        <% if $Link %></a><% end_if %>
                    </div>
                    <p class="sponsoritem__description ms-1 px2 md-px3 ">$Description</p>
                    <p class="sponsoritem__link ms-1 px2 md-px3">$Link</p>
                  </div>
                </div>
            <% end_loop %>
        </div>
      </section>
    <% end_loop %>
</div>
