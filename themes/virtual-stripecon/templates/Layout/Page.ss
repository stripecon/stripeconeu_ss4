<% include Header %>

<main class="container pt4">
    <h1>$Title</h1>

    $Content
    $Form
</main>

<% include Footer %>
