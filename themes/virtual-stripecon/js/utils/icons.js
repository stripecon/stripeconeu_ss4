import Vue from 'vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core';
import { faTwitter, faLinkedin, faFacebook, faGithub } from '@fortawesome/free-brands-svg-icons';
import { faArrowRight, faHeart, faCoffee, faEnvelope, faLink, faTimes, faTicketAlt } from '@fortawesome/free-solid-svg-icons';

library.add(faArrowRight, faHeart, faCoffee, faEnvelope, faTwitter, faLinkedin, faFacebook, faGithub, faLink, faTimes, faTicketAlt);

Vue.component('fa-icon', FontAwesomeIcon);
