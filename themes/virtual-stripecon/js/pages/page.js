import Vue from 'vue';
import 'utils/icons';

require('styles/pages/page.less');

Vue.config.productionTip = false;

new Vue({
  el: '#app'
});
