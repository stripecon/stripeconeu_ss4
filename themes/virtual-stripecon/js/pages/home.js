import Vue from 'vue';
import VueScrollTo from 'vue-scrollto';
import 'utils/icons';
import particleConfig from 'utils/particle-config';
import Modal from 'components/modal';

Vue.use(VueScrollTo);

const data = typeof payload !== 'undefined' ? { ...payload } : {};

require('styles/pages/home.less');
require('particles.js');

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  data() {
    return {
      ...data,
      speaker: null,
      activeDayId: null
    };
  },
  mounted() {
    this.$nextTick(() => {
      particlesJS('particle-bg', particleConfig);
    });

    if (this.days && Array.isArray(this.days)) {
      const today = this.days.find((day) => day.isToday);
      this.activeDayId = today ? today.id : this.days[0].id;
    }
  },
  methods: {
    setSpeakerById(id) {
      this.speaker = this.speakers.find((s) => s.id === id);
    },
    setDay(id) {
      this.activeDayId = id;
    }
  },
  components: { Modal }
});
