<div class="content-element__schedule<% if $Style %> $StyleVariant<% end_if %> text-center">
<% if $ShowTitle %>
        <h2 class="speakers-element__title">
            $Title
        </h2>
<% end_if %>
$HTML
    <section class="container">
        <div class="flex flex-wrap -mx2 <% if $Days.Count <= 2 %>center-items<% end_if %>">
            <% loop $Days.Sort('Date ASC') %>
                <% include Netwerkstatt/Block/Schedule/Day %>
            <% end_loop %>
        </div>
    </section>
</div>