<div class="content-element__sponsors<% if $Style %> $StyleVariant<% end_if %> text-center">
    <% if $ShowTitle %>
        <h2 class="sponsors-element__title">
            $Title
        </h2>
    <% end_if %>
    $HTML

    <% loop $AllActiveSponsors.GroupedBy('LevelID') %>



    <section class="container">
        <h3 class="text-center">$Children.First.Level.Title</h3>
        <div class="flex flex-wrap -mx2 <% if $Children.Count <= 2 %>center-items<% end_if %>">

            <% loop $Children.Sort('TitleUppercase') %>
      
                <div class="md:px-2 sponsoritem sponsoritem--$SponsorLevelSuffix">
                    <h4 class="sponsoritem__title">
                    <% if $Link %><a href="$Link.URL"><% end_if %>
                        $Title
                    <% if $Link %></a><% end_if %>
                    </h4>
                    <div class="sponsoritem__logo">
                        <% if $Link %><a href="$Link.URL"><% end_if %>
                        $Logo.FitMax(520,340)
                        <% if $Link %></a><% end_if %>
                    </div>
                    <p class="sponsoritem__description text-sm px-2 md:px-8 ">$Description</p>
                    <p class="sponsoritem__link text-sm px-2 md:px-8">$Link</p>
                </div>
            <% end_loop %>
        </div>
    </section>
    <% end_loop %>
</div>
