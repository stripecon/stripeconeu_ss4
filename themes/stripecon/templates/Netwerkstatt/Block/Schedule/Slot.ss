<% if $Talks.Count == 0 %>
    <% include Netwerkstatt/Block/Schedule/Slot_NoTalk %><%-- No talk: e.g. lunch break --%>
<% else_if $Talks.Count == 1 %>
    <% include Netwerkstatt/Block/Schedule/Slot_Single %><%-- one talk: normal talk --%>
<% else %>
    <% include Netwerkstatt/Block/Schedule/Slot_Multi %><%-- many talks: e.g. lightning talks --%>
<% end_if %>
