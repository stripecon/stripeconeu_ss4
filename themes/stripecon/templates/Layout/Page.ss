<main class="max-w-2xl mx-auto px-2 xl:px-0 single">
    <h1>$Title</h1>
    $Content
    $Form
</main>
