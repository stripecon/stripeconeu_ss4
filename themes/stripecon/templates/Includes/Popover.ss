<template id="contact-template" xmlns="http://www.w3.org/1999/html">
    <div class="follow-box">
        <h5>Follow us</h5>
        <p>
            <a href="https://www.facebook.com/StripeConEU/" class="follow-box--link">{$SVG('facebook').extraClass('h-5 fill-current')} Facebook</a>
            <a href="https://twitter.com/StripeConEU" class="follow-box--link">{$SVG('twitter').extraClass('h-5 fill-current')} Twitter</a>
        </p>

        <hr>

        <h5>Get in Contact</h5>
        <p>
            On Slack: silverstripe-user.slack.com<br>
            Channel: stripeconeu<br>
            Per Mail: <a href="mailto:info@stripecon.eu">info@stripecon.eu</a>
        </p>
        <hr>
        <h5>Or register to our newsletter</h5>
        <div id="mc_embed_signup">
            <form action="https://lemon8.us18.list-manage.com/subscribe/post?u=dd989cb1dcb97a5f718e3eb3b&amp;id=cb4eff3b74"
                  method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
                  target="_blank" novalidate>
                <div id="mc_embed_signup_scroll " class="flex items-center">
                    
                    <input type="email" value="" name="EMAIL" class="email appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="mce-EMAIL" placeholder="email address"
                           required>
                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                                                                              name="b_dd989cb1dcb97a5f718e3eb3b_cb4eff3b74"
                                                                                              tabindex="-1" value="">
                    </div>
                    <input type="submit" value="Go" name="subscribe" id="mc-embedded-subscribe" class="button btn btn--transparent inline-block">
                </div>
            </form>
        </div>
    </div>
</template>