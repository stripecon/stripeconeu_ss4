<nav class="navigation navigation--main">
   
    <ul class="navigation__list">
        <% loop $MenuSet('main') %>
            <li class="navigation__list-item  navigation__list-item--$LinkingMode inline-block">
                {$Me}
            </li>
        <% end_loop %>
        <li  class="navigation__list-item  navigation__list-item--$LinkingMode inline-block">
        	<span id="contact">Follow us</span>
    	</li>
    </ul>
</nav>
<%--<nav class="navigation navigation--main navigation--secondary container">--%>
<%--    <% loop $Menu(1) %>--%>
<%--        <% if $Children %>--%>
<%--            <ul class="navigation__list <% if $isSection %><% else %>hidden<% end_if %>">--%>
<%--                <% loop $Children %>--%>
<%--                    <li class="navigation__list-item  navigation__list-item--secondary navigation__list-item--$LinkingMode hidden md:inline-block"><a href="$Link">$MenuTitle</a></li>--%>
<%--                <% end_loop %>--%>
<%--            </ul>--%>
<%--        <% end_if %>--%>
<%--    <% end_loop %>--%>
<%--</nav>--%>