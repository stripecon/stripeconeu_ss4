<%--<div class="aspect-ratio aspect-ratio--bottom-21-9">--%>
    <%--<img src="https://dummyimage.com/320x180/0a0/fff&text=21to9" alt="">--%>
<%--</div>--%>
<header class="bg-secondary w-full p-4 page-header">
<div class="my-4 max-w-6xl mx-auto flex justify-between">
    <span class="logo">{$SVG('stripecon_19_white').extraClass('inline-block align-middle')}</span>
    <% include Navigation %>
</div>
</header>