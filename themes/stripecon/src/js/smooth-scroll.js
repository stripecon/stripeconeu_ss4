import SmoothScroll from 'smooth-scroll/dist/smooth-scroll.polyfills.min';
/** we got it from here https://github.com/cferdinandi/smooth-scroll */

document.addEventListener("DOMContentLoaded", function () {
    var scroll = new SmoothScroll('a[href*="#"]', {
        speed: 500,
        speedAsDuration: true
    });
});
