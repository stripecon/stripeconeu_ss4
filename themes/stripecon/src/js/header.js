document.addEventListener("DOMContentLoaded", function (e) { 
    const header = document.querySelector('header');
    window.onscroll = (e) => {
        if (window.scrollY > 730) {
            
            header.classList.add('ex-js-fixed');
        } else {
            header.classList.remove('ex-js-fixed');
        }
    }
});



