import tippy from 'tippy.js/esm/index.all.min';

/** got it from here https://atomiks.github.io/tippyjs/html-content/ */
document.addEventListener("DOMContentLoaded", function () {
    const template = document.getElementById('contact-template');
    const container = document.createElement('div');

    container.appendChild(document.importNode(template.content, true));

    /** full list of options https://atomiks.github.io/tippyjs/all-options/ */
    tippy('#contact', {
        interactive: true,
        trigger: 'click',
        content: container.innerHTML,
        onMount({ reference }) {
            reference.setAttribute('aria-expanded', 'true')
        },

        onHide({ reference }) {
            reference.setAttribute('aria-expanded', 'false')
        },
    })
});