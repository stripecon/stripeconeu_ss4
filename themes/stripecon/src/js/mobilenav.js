ready(function () {
    document.getElementById('hamburger').addEventListener('click', function () {
        toggleClass(document.querySelector('body'), 'mobile-nav-active');
    });
    var trigger =  document.querySelectorAll('.trigger');

    var toggleExpanded = function() {
        toggleClass(this.nextElementSibling, 'expanded');
        var parent = this.closest('li');
        if (parent !== undefined) {
            toggleClass(parent, 'expanded');
        }

    };

    [].forEach.call(trigger, function (trigger) {
        trigger.addEventListener("click", toggleExpanded, false);
    });

    document.getElementById('overlaynav').addEventListener("click", function (event) {
        event.stopPropagation();
        toggleClass(document.querySelector('body'), 'mobile-nav-active');
    });
    // document.querySelector("#overlaynav nav").addEventListener("click", function (event) {
    //     event.stopPropagation();
    // });
    //
    // document.querySelectorAll("#overlaynav a").addEventListener("click", function (event) {
    //     $('body').fadeTo("fast", 0.6);
    // });
});


function ready(fn) {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading") {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

function toggleClass(el, className) {
    if (el.classList) {
        el.classList.toggle(className);
    } else {
        var classes = el.className.split(' ');
        var existingIndex = classes.indexOf(className);

        if (existingIndex >= 0)
            classes.splice(existingIndex, 1);
        else
            classes.push(className);

        el.className = classes.join(' ');
    }
}




