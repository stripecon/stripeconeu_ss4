const path = require("path");
const LiveReloadPlugin = require('webpack-livereload-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const nodeSassMagicImporter = require('node-sass-magic-importer');

const GoogleFontsPlugin = require("@beyonk/google-fonts-webpack-plugin");

const exec = require('child_process').exec;
const ComposerVendorExpose = 'php C:\\Users\\Werner\\bin\\composer.phar vendor-expose -d ../../';

module.exports = {
    entry: {
        app: ['./src/js/entry.js', './src/css/main.scss'],
    },
    output: {
        filename: "js/[name].js",
        path: path.resolve(__dirname, "./dist"),
    },
    mode: 'development',
    watch: true,
    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    },

                    {
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',
                            sourceMap: true,
                            // plugins: [
                            //     // require('autoprefixer')({'browsers': ['> 1%', 'last 2 versions']}),
                            // ]
                        }
                    },
                    {
                        loader: 'resolve-url-loader',
                    },                    {
                        loader: 'sass-loader',
                        options: {
                            importer: nodeSassMagicImporter(),
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['babel-preset-env']
                    }
                }
            },
            {
                test: /\.(png|svg|jpe?g|gif)$/,
                loader:  'file-loader',
                options: {
                    name:  'images/[name].[ext]',
                    publicPath: '/resources/themes/stripecon/dist/',
                }

            }
        ]
    },
    plugins: [
        new LiveReloadPlugin({
            protocol: 'http',
            hostname: '127.0.0.1',
            appendScriptTag: true
        }),

        
/*        {
            apply: (compiler) => {
                compiler.hooks.afterEmit.tap('AfterEmitPlugin', (compilation) => {
                    exec(ComposerVendorExpose, (err, stdout, stderr) => {
                        if (stdout) process.stdout.write(stdout);
                        if (stderr) process.stderr.write(stderr);
                    });
                });
            }
        },*/
        
        // new CleanWebpackPlugin(['dist']),
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
            chunkFilename: "[id].css"
        })

        ,
        new GoogleFontsPlugin({
            fonts: [
                { family: "Rubik", variants: [ "regular", "700" ] }
            ],
            filename: 'css/fonts.css',
            path: 'fonts/'
        })
    ],
}