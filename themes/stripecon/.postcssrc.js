module.exports = {
    plugins: [
        require("tailwindcss")('./tailwind.config.js'),
        require("postcss-color-function"),
        require('postcss-custom-properties')(),
        require("autoprefixer")()
    ]
};