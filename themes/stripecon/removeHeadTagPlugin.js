/**
 * Kudos to Gregor Weindl for this plugin
 * @constructor
 */

function RemoveHeadTagPlugin() {
}


RemoveHeadTagPlugin.prototype.apply = function (compiler) {

    compiler.hooks.compilation.tap('RemoveHeadTagPlugin', (compilation) => {

        compilation.hooks.htmlWebpackPluginAfterHtmlProcessing.tapAsync(
            'RemoveHeadTagPlugin',

            (data, cb) => {

                data.html = data.html.replace(/(<\/head>)/i, '')

                cb(null, data)

            }
        )

    })

}


module.exports = RemoveHeadTagPlugin;