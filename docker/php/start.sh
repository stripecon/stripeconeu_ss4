#!/bin/bash
cd /var/www/html
chown -R www-data:www-data public/assets
chmod -R 755 public/assets
composer install
./vendor/silverstripe/framework/sake installsake
sake dev/build flush=all
php-fpm
