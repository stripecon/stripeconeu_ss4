<?php

namespace {

    use Level51\Util\RequirementsProxy;
    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\Control\Director;

    class PageController extends ContentController
    {
        /**
         * An array of actions that can be accessed via a request. Each array element should be an action name, and the
         * permissions or conditions required to allow the user to access it.
         *
         * <code>
         * [
         *     'action', // anyone can access this action
         *     'action' => true, // same as above
         *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
         *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
         * ];
         * </code>
         *
         * @var array
         */
        private static $allowed_actions = [];

        protected function init()
        {
            parent::init();
            $this->requireAssets();
            // You can include any CSS or JS required by your project here.
            // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/
        }

        protected function requireAssets() {
            RequirementsProxy::requireAsset($this->isHome() ? 'home' : 'page');
        }

        public function isHome() {
            $rootSegment = explode('/', $this->getRequest()->getURL())[0];

            return $rootSegment === 'home';
        }

        /**
         * Template helper
         *
         * @return bool
         */
        public function getIsDev()
        {
            return Director::isDev();
        }
    }
}
