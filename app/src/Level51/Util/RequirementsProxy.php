<?php

namespace Level51\Util;

use Psr\SimpleCache\InvalidArgumentException;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\View\Requirements;

class RequirementsProxy {

    /**
     * Require an asset by name, the file extension will be appended automatically.
     *
     * On non dev environments the package version number is appended.
     *
     * @param string $name The name of the asset file to require (e.g. home).
     * @param bool   $cssOnly Loads only the css file
     */
    public static function requireAsset($name, $cssOnly = false) {
        $theme = Util::getActiveTheme();
        $path = "/resources/themes/$theme/build/";

        // Add package version string on non-dev env
        if (!Director::isDev()) {
            try {
                $name .= '.' . Util::getThemePackageVersion();
            } catch (InvalidArgumentException $e) {
            }
        }

        if (!$cssOnly)
            Requirements::javascript(Controller::join_links($path, $name . '.js'));

        Requirements::css(Controller::join_links($path, $name . '.css'));
    }
}
