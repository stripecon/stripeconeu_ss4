<?php

namespace Level51\Util;

use Psr\SimpleCache\CacheInterface;
use SilverStripe\Control\Director;
use SilverStripe\Core\Cache\DefaultCacheFactory;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Flushable;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\View\SSViewer;

class Util implements Flushable {

    /**
     * @return CacheInterface
     */
    private static function getCache() {
        return Injector::inst()->get(DefaultCacheFactory::class)->create(self::class, [
            'directory' => Director::baseFolder() . DIRECTORY_SEPARATOR . 'cache'
        ]);
    }

    /**
     * Get version from the package.json file.
     *
     * @return string
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public static function getThemePackageVersion() {
        $cache = self::getCache();

        if (!$cache->has('themePackageVersion')) {
            $cache->set('themePackageVersion', self::getVersionCode());
        }

        return $cache->get('themePackageVersion') ?? self::getVersionCode();
    }

    private static function getVersionCode() {
        $pkg = json_decode(file_get_contents('../themes/' . self::getActiveTheme() . '/package.json'));

        return $pkg->version;
    }

    /**
     * Returns the active Maintheme.
     *
     * @return mixed
     */
    public static function getActiveTheme() {
        return Config::inst()->get(SSViewer::class, 'themes')[0];
    }

    /**
     * Flush the namespaced cache.
     */
    public static function flush() {
        $cache = self::getCache();
        $cache->clear();
    }
}
