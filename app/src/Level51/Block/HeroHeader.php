<?php

namespace Level51\Block;

use DNADesign\Elemental\Models\ElementContent;
use gorriecoe\Link\Models\Link;
use SilverShop\HasOneField\HasOneButtonField;
use SilverStripe\Forms\FieldList;

class HeroHeader extends ElementContent {

    private static $singular_name = 'Hero Header block';

    private static $plural_name = 'Hero Header blocks';

    private static $description = 'Block for big header with logo and event teaser';

    private static $has_one = [
        'CTALink' => Link::class
    ];

    public function getType() {
        return _t(__CLASS__ . '.BlockType', 'HeroHeader');
    }

    private static $inline_editable = false;

    public function getCMSFields() {
        $this->beforeUpdateCMSFields(function (FieldList $fields) {
            if ($this->ID) {
                $fields->replaceField('CTALinkID', HasOneButtonField::create($this, 'CTALink'));
            } else {
                $fields->removeByName('CTALinkID');
            }
        });

        return parent::getCMSFields();
    }
}
