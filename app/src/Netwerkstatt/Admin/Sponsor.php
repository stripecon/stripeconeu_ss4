<?php


namespace Netwerkstatt\Admin;


use Netwerkstatt\Model\Sponsor as SponsorModel;
use Netwerkstatt\Model\SponsorLevel;
use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldSortableHeader;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class Sponsor extends ModelAdmin
{
    private static $managed_models = [
        SponsorModel::class,
        SponsorLevel::class
    ];

    private static $url_segment = 'sponsors';
    private static $menu_title = 'Sponsors';

    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);

        $model = singleton($this->modelClass);
        $gridField = $form->Fields()->dataFieldByName($this->sanitiseClassName($this->modelClass));

        /** add sorting if we have a field for... */
        if (class_exists(GridFieldOrderableRows::class)
            && $model->hasField('SortOrder')
        ) {
            if ($gridField instanceof GridField) {
                $gridField->getConfig()->addComponent(new GridFieldOrderableRows('SortOrder'));
            }
        }

        //sort Level Title by SponsorLevel SortOrder
        if ($gridField instanceof GridField) {
            $gridField->getConfig()->getComponentByType(GridFieldSortableHeader::class)
                ->setFieldSorting([
                    'Level.Title' => 'Level.SortOrder'
                ]);
        }

        return $form;
    }

    public function getList()
    {
        $list = parent::getList();

        if ($this->modelClass === SponsorModel::class) {
            $list = $list->sort([
                'Level.SortOrder' => 'ASC',
                'Title' => 'ASC'
            ]);
        }

        return $list;
    }
}