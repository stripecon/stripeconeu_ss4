<?php


namespace Netwerkstatt\Admin;


use Netwerkstatt\Model\Day;
use Netwerkstatt\Model\Slot;
use Netwerkstatt\Model\Speaker;
use Netwerkstatt\Model\Talk;
use SilverStripe\Admin\ModelAdmin;

class Schedule extends ModelAdmin
{
    private static $url_segment = 'schedule';
    private static $menu_title = 'Schedule';

    private static $managed_models = [
        Talk::class,
        Speaker::class,
        Slot::class,
        Day::class
    ];

    public function getList()
    {
        $list =  parent::getList();

        if ($this->modelClass === Talk::class) {
            $list = $list->sort([
                'Slot.Day.Date' => 'ASC',
                'Slot.Time' => 'ASC'
            ]);
        }

        return $list;
    }


}