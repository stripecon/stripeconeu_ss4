<?php


namespace Netwerkstatt\Block;


use DNADesign\Elemental\Models\ElementContent;
use Netwerkstatt\Model\Sponsor;
use SilverStripe\ORM\GroupedList;

class Sponsors extends ElementContent
{
    private static $singular_name = 'Sponsor block';

    private static $plural_name = 'Sponsor blocks';

    private static $description = 'Block listing all Sponsors';

    public function getType()
    {
        return _t(__CLASS__ . '.BlockType', 'Sponsors');
    }

    public function getAllActiveSponsors()
    {
        $allActiveSponsors = Sponsor::get()
            ->filter(['IsActive' => 1])
            ->sort([
                'Level.SortOrder' => 'ASC',
            ]);

        return GroupedList::create($allActiveSponsors);
    }
}