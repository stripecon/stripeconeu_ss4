<?php


namespace Netwerkstatt\Block;


use DNADesign\Elemental\Models\ElementContent;
use Level51\PayloadInjector\PayloadInjector;
use Level51\PayloadInjector\PayloadInjectorException;
use Netwerkstatt\Model\Speaker;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridFieldAddExistingAutocompleter;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;
use SilverStripe\Versioned\GridFieldArchiveAction;
use Symbiote\GridFieldExtensions\GridFieldAddExistingSearchButton;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class Speakers extends ElementContent
{
    private static $many_many = [
        'Speakers' => Speaker::class
    ];

    private static $many_many_extraFields = [
        'Speakers' => [
            'SortOrder' => 'Int'
        ]
    ];

    private static $singular_name = 'Speaker block';

    private static $plural_name = 'Speaker blocks';

    private static $description = 'Block listing some selected speakers';

    private static $table_name = 'Speakers';

    private static $owns = [
        'Speakers',
    ];

    private static $inline_editable = false;

    public function getCMSFields()
    {
        $this->beforeUpdateCMSFields(function (FieldList $fields) {

            if ($this->ID) {

                /** @var \SilverStripe\Forms\GridField\GridField $speakerField */
                $speakerField = $fields->dataFieldByName('Speakers');
//                $fields->removeByName('Promos');
                $config = $speakerField->getConfig();
                $config->removeComponentsByType([
                    GridFieldAddExistingAutocompleter::class,
                    GridFieldDeleteAction::class,
                    GridFieldArchiveAction::class,
                ])->addComponents(
                    new GridFieldOrderableRows('SortOrder'),
                    new GridFieldAddExistingSearchButton()
                );

//                $fields->addFieldsToTab('Root.Main', array(
//                    $speakerField,
//                ));
            }
        });

        //ugly hack, cause ElementContent would overwrite beforeUpdateCMSFields
        return parent::getCMSFields();
    }

    public function getType()
    {
        return _t(__CLASS__ . '.BlockType', 'Speakers');
    }

    public function forTemplate($holder = true) {
        $payload = [];
        /** @var Speaker $speaker */
        foreach (Speaker::get() as $speaker) {
            $payload[] = $speaker->getPayload();
        }

        try {
            PayloadInjector::singleton()->stage([
                'speakers' => $payload
            ]);
        } catch (PayloadInjectorException $e) {
        }

        return parent::forTemplate($holder);
    }
}
