<?php


namespace Netwerkstatt\Block;


use DNADesign\Elemental\Models\ElementContent;
use Level51\PayloadInjector\PayloadInjector;
use Level51\PayloadInjector\PayloadInjectorException;
use Netwerkstatt\Model\Day;

class Schedule extends ElementContent {

    private static $many_many = [
        'Days' => Day::class
    ];

    private static $singular_name = 'Schedule block';

    private static $plural_name = 'Schedule blocks';

    private static $description = 'Block listing a schedule of one or more days';

    private static $table_name = 'Schedule';
    private static $inline_editable = false;

    public function getType() {
        return _t(__CLASS__ . '.BlockType', 'Schedule');
    }

    public function forTemplate($holder = true) {
        $payload = [
            'days' => []
        ];

        foreach ($this->Days()->sort('Date', 'ASC') as $day) {
            $payload['days'][] = [
                'id'      => $day->ID,
                'date'    => $day->Date,
                'isToday' => $day->Date && $day->dbObject('Date')->isToday()
            ];
        }

        try {
            PayloadInjector::singleton()->stage($payload);
        } catch (PayloadInjectorException $e) {
        }

        return parent::forTemplate($holder);
    }
}
