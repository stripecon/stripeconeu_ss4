<?php


namespace Netwerkstatt\Model;


use SilverStripe\Forms\TimeField;
use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\DataObject;
use SilverStripe\View\ArrayData;

class Slot extends DataObject
{
    private static $db = [
        'Title' => 'Varchar',
        'Time' => 'Time',
        'Description' => 'HTMLText',
        'InProgress' => 'Boolean',
        'IsLunch' => 'Boolean',
        'IsCoffee' => 'Boolean',
        'IsParty' => 'Boolean',
        'IsLightningTalks' => 'Boolean',
        'IsWalk' => 'Boolean',
    ];

    private static $has_one = [
        'Day' => Day::class
    ];

    private static $has_many = [
        'Talks' => Talk::class
    ];

    private static $summary_fields = [
        'Title',
        'Day.Date',
        'Time',
    ];

    private static $table_name = 'Slot';

    private static $default_sort = 'Time ASC';

    private static $singluar_name = 'Slot';
    private static $plural_name = 'Slots';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        /** @var TimeField $time */
        $time = $fields->dataFieldByName('Time');
        $time
//            ->setHTML5(false)
            ->setCalendarConfig([
                'time_24hr' => true,
                'defaultHour' => 12
            ]);

        return $fields;
    }

    public function getTalkTypes() {
        if ($this->Talks() && $this->Talks()->exists()) {
            $types = $this->Talks()->column('Type');

            return ArrayList::create(array_map(function($type) {
                return ArrayData::create([
                    'Type' => $type
                ]);
            }, array_unique($types)));
        }

        return null;
    }
}
