<?php


namespace Netwerkstatt\Model;


use gorriecoe\Link\Models\Link;
use SilverShop\HasOneField\HasOneButtonField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;

class Talk extends DataObject {
    private static $db = [
        'Title'            => 'Varchar',
        'ShortDescription' => 'Text',
        'Type'             => 'Varchar',
        'SortOrder'        => 'Int',
    ];

    private static $has_one = [
        'Slot'      => Slot::class,
        'SlideLink' => Link::class,
        'VideoLink' => Link::class,
    ];

    private static $many_many = [
        'Speakers' => Speaker::class
    ];

    private static $summary_fields = [
        'Title',
        'Slot.Day.Date',
        'Slot.Time',
        'SpeakersSummary',
        'HasSlideLinked.Nice',
        'HasVideoLinked.Nice' => 'Has Video'
    ];

    private static $searchable_fields = [
        'Title',
        'Slot.Day.Date'

    ];

    private static $field_labels = [
        'Date'                => 'Date',
        'Slot.Day.Date'       => 'Date',
        'Slot.Time'           => 'Time',
        'SpeakersSummary'     => 'Speaker(s)',
        'HasSlideLinked.Nice' => 'Has Slides',
        'HasVideoLinked.Nice' => 'Has Video'
    ];

    private static $casting = [
        'HasSlideLinked'  => 'Boolean',
        'HasVideoLinked'  => 'Boolean',
        'SpeakersSummary' => 'Varchar'
    ];

    private static $table_name = 'Talk';

    private static $default_sort = [
        'SortOrder' => 'ASC'
    ];

    private static $singluar_name = 'Talk';
    private static $plural_name = 'Talks';

    private static $default_types = [
        'Talk',
        'Lightning Talk',
        'Workshop',
        'Case Study',
        'Session'
    ];

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->removeByName('SortOrder');

        $slideLinkField = HasOneButtonField::create($this, 'SlideLink');
        $fields->replaceField('SlideLinkID', $slideLinkField);

        $videoLinkField = HasOneButtonField::create($this, 'VideoLink');
        $fields->replaceField('VideoLinkID', $videoLinkField);

        $typeOptions = array_reduce(self::config()->get('default_types'), function($arr, $current) {
            $arr[$current] = $current;

            return $arr;
        }, []);
        $fields->replaceField('Type', DropdownField::create('Type', 'Type', $typeOptions));

        return $fields;
    }

    public function getSpeakersSummary() {
        if ($this->Speakers()->count() === 0) {
            return '';
        }

        return implode(', ', $this->Speakers()->Column('Name'));
    }

    public function getHasSlideLinked() {
        return $this->SlideLinkID && $this->SlideLink() instanceof Link;
    }

    public function getHasVideoLinked() {
        return $this->VideoLinkID && $this->VideoLink() instanceof Link;
    }

    public function getPayload() {
        return [
            'id'    => $this->ID,
            'title' => $this->Title,
            'type'  => $this->Type
        ];
    }
}
