<?php


namespace Netwerkstatt\Model;


use gorriecoe\Link\Models\Link;
use SilverShop\HasOneField\HasOneButtonField;
use SilverStripe\Assets\Image;
use SilverStripe\ORM\DataObject;

class Sponsor extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(128)',
        'Description' => 'Text',
        'IsActive' => 'Boolean'
    ];

    private static $has_one = [
        'Link' => Link::class,
        'Logo' => Image::class,
        'Level' => SponsorLevel::class
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Logo',
    ];

    private static $summary_fields = [
        'Title',
        'Level.Title',
        'IsActive.Nice',
        'HasLogo.Nice'
    ];

    private static $searchable_fields = [
        'Title',
        'IsActive'
    ];

    private static $field_labels = [
        'Level.Title' => 'Sponsor Level',
        'IsActive.Nice' => 'Is Active',
        'HasLogo.Nice' => 'Has Logo'
    ];

    private static $casting = [
        'HasLogo' => 'Boolean'
    ];

    private static $table_name = 'Sponsor';

    private static $default_sort = 'Title ASC';

    private static $singluar_name = 'Sponsor';
    private static $plural_name = 'Sponsors';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName(['SortOrder']);

        $linkField = HasOneButtonField::create($this, 'Link');

        $fields->replaceField('LinkID', $linkField);

        return $fields;
    }

    /**
     * Helper for CSS classes
     *
     * @return string
     */
    public function getSponsorLevelSuffix()
    {
        if (!$this->LevelID) {
            return '';
        }

        return $this->Level()->getSponsorLevelSuffix();
    }

    public function getHasLogo()
    {
        return $this->LogoID && $this->Logo()->getIsImage();
    }

    /**
     * Helper for sorting in Templates
     *
     * @return string
     */
    public function getTitleUppercase()
    {
        return strtoupper($this->Title);
    }
}