<?php


namespace Netwerkstatt\Model;


use gorriecoe\Link\Models\Link;
use Netwerkstatt\Block\Speakers;
use SilverShop\HasOneField\HasOneButtonField;
use SilverStripe\Assets\Image;
use SilverStripe\ORM\DataObject;

/**
 * Class Speaker
 * @package Netwerkstatt\Model
 *
 * @property string $TwitterHandle
 * @property string $LinkedInProfile
 * @property string $GitHubHandle
 * @property string $Website
 */
class Speaker extends DataObject {
    private static $db = [
        'Name'             => 'Varchar',
        'ShortDescription' => 'Text',
        'TwitterHandle'    => 'Varchar',
        'LinkedInProfile'  => 'Varchar',
        'GitHubHandle'     => 'Varchar',
        'Website'          => 'Varchar',
        'Company'          => 'Varchar'
    ];

    private static $has_one = [
        'ProfileImage' => Image::class,
        'Link'         => Link::class,
    ];

    private static $belongs_many_many = [
        'Talks'         => Talk::class,
        'SpeakerBlocks' => Speakers::class
    ];

    private static $owns = [
        'ProfileImage'
    ];

    private static $summary_fields = [
        'ProfileImage.StripThumbnail' => 'Profile Image',
        'Name'                        => 'Name',
        'HasProfileImage.Nice'        => 'Has Profile Image',
    ];

    private static $searchable_fields = [
        'Name',
    ];

    private static $field_labels = [
        'LinkedInProfile' => 'LinkedIn Profile',
        'GitHubHandle'    => 'GitHub Handle'
    ];

    private static $casting = [
        'HasProfileImage' => 'Boolean',
    ];

    private static $table_name = 'Speaker';

    private static $singluar_name = 'Speaker';
    private static $plural_name = 'Speakers';

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $linkField = HasOneButtonField::create($this, 'Link');
        $fields->replaceField('LinkID', $linkField);

        return $fields;
    }


    public function getHasProfileImage() {
        return $this->ProfileImageID && $this->ProfileImage()->getIsImage();
    }

    public function TwitterLink() {
        return 'https://twitter.com/' . str_replace('@', '', $this->TwitterHandle);
    }

    public function getPayload() {
        $talks = [];
        /** @var Talk $talk */
        foreach ($this->Talks() as $talk) {
            $talks[] = $talk->getPayload();
        }

        return [
            'id'          => $this->ID,
            'name'        => $this->Name,
            'company'     => $this->Company,
            'description' => $this->ShortDescription,
            'image'       => $this->ProfileImage()->Fill(400, 400)->Link(),
            'twitter'     => $this->TwitterHandle,
            'linkedIn'    => $this->LinkedInProfile,
            'github'      => $this->GitHubHandle,
            'website'     => $this->Website,
            'talks'       => $talks
        ];
    }
}
