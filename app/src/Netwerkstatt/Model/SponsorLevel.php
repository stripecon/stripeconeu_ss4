<?php


namespace Netwerkstatt\Model;


use SilverStripe\ORM\DataObject;
use SilverStripe\View\Parsers\URLSegmentFilter;

class SponsorLevel extends DataObject
{
    private static $db = [
        'Title' => 'Varchar(64)',
        'ExtraCSS' => 'Varchar(255)',
        'SortOrder' => 'Int'
    ];

    private static $has_many = [
        'Sponsors' => Sponsor::class
    ];

    private static $default_sort = 'SortOrder';

    private static $table_name = 'SponsorLevel';

    private static $singluar_name = 'Sponsor Level';
    private static $plural_name = 'Sponsor Levels';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName(['SortOrder']);

        return $fields;
    }

    /**
     * Helper for CSS classes
     * @return string
     */
    public function getSponsorLevelSuffix(): string
    {
        return URLSegmentFilter::singleton()->filter($this->Title);
    }
}