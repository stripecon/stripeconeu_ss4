<?php


namespace Netwerkstatt\Model;


use Netwerkstatt\Block\Schedule;
use SilverStripe\ORM\DataObject;

class Day extends DataObject
{
    private static $db = [
        'Title' => 'Varchar',
        'Date' => 'Date',
        'Description' => 'HTMLText'
    ];

    private static $has_many = [
        'Slots' => Slot::class
    ];

    private static $belongs_many_many = [
        'Schedules' => Schedule::class
    ];

    private static $table_name = 'Day';

    private static $default_sort = 'Date ASC';

    private static $singluar_name = 'Day';
    private static $plural_name = 'Days';

}