<?php

namespace Netwerkstatt\Page;


class ElementalPage extends \Page
{
    private static $singular_name = 'Page with Elemental Blocks';

    private static $plural_name = 'Pages with Elemental Blocks';

    private static $table_name = 'ElementalPage';
}
