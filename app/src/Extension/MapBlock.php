<?php


namespace Netwerkstatt\Extension;


use Bigfork\SilverStripeMapboxField\MapboxField;
use EdgarIndustries\ElementalMap\Provider\Mapbox;
use SilverStripe\Forms\FieldGroup;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;

class MapBlock extends DataExtension
{

    private static $defaults = [
        'Provider' => Mapbox::class
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->removeByName([
            'Centre',
            'Provider'
        ]);

        $centreField = FieldGroup::create(
            'Centre',
            MapboxField::create('LocationMap', 'Choose a location', 'DefaultLatitude', 'DefaultLongitude'),
            TextField::create('DefaultZoom', 'Zoom')
        );

        $fields->insertBefore('Size', $centreField);
        $fields->addFieldToTab('Root.Main', HiddenField::create('Provider', Mapbox::class));
    }

}