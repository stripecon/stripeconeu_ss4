<?php

namespace Netwerkstatt\Extension;


use Bigfork\SilverStripeMapboxField\MapboxField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;

class MapMarker extends DataExtension
{

    public function updateCMSFields(FieldList $fields)
    {
        $fields->removeByName(['Latitude', 'Longitude', 'Position', 'LocationMap']);
        $fields->insertAfter('Description',
            MapboxField::create('LocationMap', 'Choose a location', 'Latitude', 'Longitude'));
    }

}