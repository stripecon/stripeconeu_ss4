<?php


namespace Netwerkstatt\Extension;


use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\ArrayLib;
use SilverStripe\ORM\DataExtension;

class ElementWidth extends DataExtension
{
    private static $db = [
        'StyleWidth' => 'Varchar(255)'
    ];

    /**
     * Vice versa than BaseElement styles - key => css to add more than one css class
     *
     * @var array
     */
    private static $styles_width = [
        'Narrow' => 'max-w-2xl',
        'Wide' => 'max-w-6xl',
        'Full' => 'w-full'
    ];

    private static $defaults = [
        'styles_width' => 'Wide'
    ];

    private static $default_width = 'max-w-5xl';

    public function updateCMSFields(FieldList $fields)
    {
        $styles = $this->owner->config()->get('styles_width');

        $values = ArrayLib::valuekey(array_keys($styles));

        $widthDropdown = DropdownField::create('StyleWidth', 'Width', $values);
        $fields->insertAfter('Style', $widthDropdown);
    }

    /**
     * for inserting width related CSS classes in the template
     *
     * @return string
     */
    public function getWidthClass()
    {
        $width = $this->owner->StyleWidth;

        $styles = $this->owner->config()->get('styles_width');

        if (array_key_exists($this->owner->StyleWidth, $styles)) {
            return $styles[$width];
        }

        return $this->owner->config()->get('default_width');
    }


}